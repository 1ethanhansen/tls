# Secure states
ALL_RED = "all_red"  # red lights+no walk sign in all directions
NS_ONLY = "ns_only"  # ns through traffic allowed (including peds), ew traffic not allowed (including pedestrian traffic)
EW_ONLY = "ew_only"  # ew through traffic allowed (including peds), ns traffic not allowed (including pedestrian traffic)
NS_LEFT_TURNS = "ns_left_turns"  # ns traffic allowed to turn left, all other lights red
EW_LEFT_TURNS = "ew_left_turns"  # ew traffic allowed to turn left, all other lights red
NS_THRU_RIGHT = "ns_thru_right"  # ns through traffic allowed, right turns allowed, no pedestrians allowed
EW_THRU_RIGHT = "ew_thru_right"  # ew trough traffic allowed, right turns allowed, no pedestrians allowed
N_ALL = "n_all"  # n all car options allowed, none other allowed
S_ALL = "s_all"  # s all car options allowed, none other allowed
E_ALL = "e_all"  # e all car options allowed, none other allowed
W_ALL = "w_all"  # w all car options allowed, none other allowed
PEDS_NO_CARS = "peds_no_cars"  # All pedestrians allowed, no cars allowed
# ALL OTHER STATES ARE UNSECURE

# Constants
NORTH = "north"
SOUTH = "south"
EAST = "east"
WEST = "west"

LEFT = "left"
RIGHT = "right"
STRAIGHT = "straight"

SW = "southwest"
SE = "southeast"
NW = "northwest"
NE = "northeast"