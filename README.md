# traffic layer security

## Description
Project for CSDS 344 (Computer Security) at Case Western Reserve University. The project simulates a 2-way intersection controlled by traffic lights, ensuring only secure states are ever reached.

## Installation
`pip install -r requirements.txt`

## Usage
`python main.py`

Then, add some cars and pedestrians to the screen and watch as they move through the simulation.

## Authors and acknowledgment
Ethan Hansen, Aniyah Shirehjini, Simon Schwartz, Gabriel Wolf

## License
All rights reserved. At this time NO PERMISSION IS GIVEN to copy, use, distribute.
