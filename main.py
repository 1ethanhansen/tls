#!/usr/bin/env python
import pygame
from math import ceil, floor
from classes import *
from graphics import *


# Constants
WIDTH, HEIGHT = 560, 560  # Adjusted for a 7 by 7 grid
GRID_SIZE = 10
CELL_SIZE = WIDTH // GRID_SIZE

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREY = (169, 169, 169)
YELLOW = (255, 255, 0)

GRASSCOLOR = "forestgreen"
ROADCOLOR = "#0E0C1D"
WALKCOLOR = "#EBEBEB"
WALKOFF = "#E53C3C"
SPACERCOLOR = "#FFE607"
CROSSBG = "#515151"
WALKWID = 50
WALKBARSPACE = 25
LANEWID = 110
LANESPACE = 8
WALKTHICK = 12
SDIM = 800
DASHLEN = 15
DASHSPACE = 25

nswalk = False
ewwalk = False

# set_ns_walksign(True) -> pedestrians can walk in the N/S direction
# set_ns_walksign(False) -> pedestrians CAN NOT walk in the N/S direction
def set_ns_walksign(state):
    global nswalk
    nswalk = state

def set_ew_walksign(state):
    global ewwalk
    ewwalk = state

def cwh(x, y, w, h):
    return pygame.Rect(x - w/2, y - h/2, w, h)

def chw(y, x, h, w):
    return pygame.Rect(x - w/2, y - h/2, w, h)

def main():
    world = World()

    # Add some random cars
    for _ in range(10):
        location = random.choice([NORTH, SOUTH, EAST, WEST])
        goal = random.choice([LEFT, RIGHT, STRAIGHT])
        car = Car(location, goal)
        world.cars.append(car)

    # Add from random pedestrians
    for _ in range(10):
        location = random.choice([NW, NE, SW, SE])
        # Determine the corresponding goal based on the selected location
        if location == NW:
            goal = random.choice([SW, NE])
        elif location == NE:
            goal = random.choice([NW, SE])
        elif location == SW:
            goal = random.choice([NW, SE])
        elif location == SE:
            goal = random.choice([SW, NE])
        pedestrian = Pedestrian(location, goal)
        world.pedestrians.append(pedestrian)

    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("TrafficLight Sim")
    clock = pygame.time.Clock()

    total_millis = 0

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return

        screen.fill(GRASSCOLOR)
        TWOLANEWID = LANEWID*2 + LANESPACE

        ewclr = WALKCOLOR
        nsclr = WALKCOLOR
        if not world.nswalk:
            nsclr = WALKOFF
        if not world.ewwalk:
            ewclr = WALKOFF
        

        # Draw the grid with numbers excluding specified numbers
        for row in range(GRID_SIZE):
            for col in range(GRID_SIZE):
                number = row * GRID_SIZE + col 
                color = BLACK

                # Determine color based on whether the number is a sidewalk, grass, or road
                backcolor = color_from_state(world.state, world.yellow_lights, number)

                # ic(number)
                # ic(backcolor)

                pygame.draw.rect(screen, backcolor, (col * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE), 0)
                pygame.draw.rect(screen, color, (col * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE), 1)

                # Calculate the center of the cell
                center_x = col * CELL_SIZE + CELL_SIZE // 2
                center_y = row * CELL_SIZE + CELL_SIZE // 2


        cars_to_kill = []

        # Update and draw cars
        for car in CarGraphic.car_graphics:
            car.move()
            car.update(screen)
            if car.killme:
                cars_to_kill.append(car)

        for car in cars_to_kill:
            CarGraphic.car_graphics.remove(car)


        pygame.display.flip()
        clock.tick(2)
        total_millis += clock.get_time()
        # ic(total_millis)
        if total_millis >= 1000:
            print("Current state:", world.state)
            print("Yellow lights:", world.yellow_lights)
            print("Cars:")
            for car in world.cars:
                print("Location:", car.location, "\t| Goal:", car.goal)
            print("Pedestrians:")
            for pedestrian in world.pedestrians:
                print("Location:", pedestrian.location, "\t| Goal:", pedestrian.goal)
            print("-" * 30)

            world.tick()
            total_millis = 0


if __name__ == "__main__":
    main()
    pygame.quit()
