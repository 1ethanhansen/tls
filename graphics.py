import pygame
import sys
import random

from constants import *

# Constants
WIDTH, HEIGHT = 560, 560  # Adjusted for a 7 by 7 grid
GRID_SIZE = 10
CELL_SIZE = WIDTH // GRID_SIZE

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREY = (169, 169, 169)
YELLOW = (255, 255, 0)
REDCOLOR = "#BF616A"
GREENCOLOR = "#A3BE8C"
YELLOWCOLOR = "#EBCB8B"

GRASSCOLOR = "forestgreen"
ROADCOLOR = "#0E0C1D"
WALKCOLOR = "#EBEBEB"
WALKOFF = "#E53C3C"
SPACERCOLOR = "#FFE607"
CROSSBG = "#515151"

northsouth_crosswalks = [12,13,14,15,16,17,82,83,84,85,86,87]
eastwest_crosswalks = [28,38,48,58,68,78,21,31,41,51,61,71]
walk_corners = [1,11,10,8,18,19,80,81,91,98,88,89]
all_sidewalks = {1,11,21,31,41,51,61,71,81,91,10,11,12,13,14,15,16,17,18,19,8,18,28,38,38,48,58,68,78,88,98,80,81,82,83,84,85,86,87,88,89}
exclude_numbers = {0, 9, 90, 99}

northleft = 24
northstraight = 23
northright = 22

southleft = 75
southstraight = 76
southright = 77

eastleft = 47
eaststraight = 37
eastright = 27

westleft = 52
weststraight = 62
westright = 72

def color_from_state(state, yellow, number):
    if number in exclude_numbers:
        return GRASSCOLOR

    elif number in walk_corners:
        return GREY

    elif state == ALL_RED:
        if number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR
        if number in [northleft, northstraight, northright, southleft, southstraight, southright, eastleft, eaststraight, eastright, westleft, weststraight, westright]:
            return REDCOLOR

    elif state == NS_ONLY:
        if number in [northstraight,southstraight] or number in eastwest_crosswalks:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, northright, southleft, southright, eastleft, eaststraight, eastright, westleft, weststraight, westright] or number in northsouth_crosswalks:
            return REDCOLOR

    elif state == EW_ONLY:
        if number in [eaststraight,weststraight] or number in northsouth_crosswalks:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, northstraight, northright, southleft, southstraight, southright, eastleft, eastright, westleft, westright] or number in eastwest_crosswalks:
            return REDCOLOR

    elif state == NS_LEFT_TURNS:
        if number in [northleft, southleft]:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northstraight, northright, southstraight, southright, eastleft, eaststraight, eastright, westleft, weststraight, westright] or number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR

    elif state == EW_LEFT_TURNS:
        if number in [eastleft, westleft]:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, northstraight, northright, southleft, southstraight, southright, eaststraight, eastright, weststraight, westright] or number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR

    elif state == NS_THRU_RIGHT:
        if number in [northstraight, southstraight, northright, southright]:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, southleft, eastleft, eaststraight, eastright, westleft, weststraight, westright] or number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR

    elif state == EW_THRU_RIGHT:
        if number in [eaststraight, weststraight, eastright, westright]:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, northstraight, northright, southleft, southstraight, southright, eastleft, westleft, weststraight] or number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR

    elif state == N_ALL:
        if number in [northleft, northstraight, northright]:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [southleft, southstraight, southright, eastleft, eaststraight, eastright, westleft, weststraight, westright] or number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR

    elif state == S_ALL:
        if number in [southleft, southstraight, southright]:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, northstraight, northright, eastleft, eaststraight, eastright, westleft, weststraight, westright] or number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR

    elif state == E_ALL:
        if number in [eastleft, eaststraight, eastright]:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, northstraight, northright, southleft, southstraight, southright, westleft, weststraight, westright] or number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR

    elif state == W_ALL:
        if number in [westleft, weststraight, westright]:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, northstraight, northright, southleft, southstraight, southright, eastleft, eaststraight, eastright] or number in northsouth_crosswalks or number in eastwest_crosswalks:
            return REDCOLOR
            
    elif state == PEDS_NO_CARS:
        if number in northsouth_crosswalks or number in eastwest_crosswalks:
            if yellow:
                return YELLOWCOLOR
            else:
                return GREENCOLOR
        elif number in [northleft, northstraight, northright, southleft, southstraight, southright, eastleft, eaststraight, eastright, westleft, weststraight, westright]:
            return REDCOLOR
    
    return ROADCOLOR

def plan(vehicle, start, turn):
    if vehicle == "car":
        if start == "north":
            if turn == "right":
                return [2, 12, 22, 21, 20]
            elif turn == "straight":
                return [3, 13, 23, 33, 43, 53, 63, 73, 83, 93]
            elif turn == "left":
                return [4, 14, 24, 34, 44, 54, 55, 56, 57, 58, 59]
            else:
                return [0]
        elif start == "east":
            if turn == "right":
                return [29, 28, 27, 17, 7]
            elif turn == "straight":
                return [39, 38, 37, 36, 35, 34, 33, 32, 31, 30]
            elif turn == "left":
                return [49, 48, 47, 46, 45, 44, 54, 64, 74, 84, 94]
            else:
                return [0]
        elif start == "south":
            if turn == "right":
                return [97, 87, 77, 78, 79]
            elif turn == "straight":
                return [96, 86, 76, 66, 56, 46, 36, 26, 16, 6]
            elif turn == "left":
                return [95, 85, 75, 65, 55, 45, 44, 43, 42, 41, 40]
            else:
                return [0]
        elif start == "west":
            if turn == "right":
                return [70, 71, 72, 82, 92]
            elif turn == "straight":
                return [60, 61, 62, 63, 64, 65, 66, 67, 68, 69]
            elif turn == "left":
                return [50, 51, 52, 53, 54, 55, 45, 35, 25, 15, 5]
            else:
                return [0]
        else:
            return [0]
    elif (vehicle == "person"):
        if (start == "northwest"):
            if (turn == "northeast"):
                return [10,11,12,13,14,15,16,17,18,19]
            elif (turn == "southwest"):
                return [1,11,21,31,41,51,61,71,81,91]
            else:
                return [0]
        elif (start == "northeast"):
            if (turn == "northwest"):
                return [19,18,17,16,15,14,13,12,11,10]
            elif (turn == "southeast"):
                return [8,18,28,38,48,58,68,78,88,98]
            else:
                return [0]
        elif (start == "southwest"):
            if (turn == "northwest"):
                return [91,81,71,61,51,41,31,21,11,1]
            elif (turn == "southeast"):
                return [80,81,82,83,84,85,86,87,88,89]
            else:
                return [0]
        elif (start == "southeast"):
            if (turn == "northeast"):
                return [98,88,78,68,58,48,38,28,18,8]
            elif (turn == "southwest"):
                return [89,88,87,86,85,84,83,82,81,80]
            else:
                return [0]
        else:
            return [0]


# CarGraphic class
class CarGraphic:

    car_graphics = []

    def __init__(self, car_number, positions):
        self.car_number = car_number
        self.positions = positions
        self.position_index = 0
        self.position = self.positions[self.position_index]
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.killme = False
        CarGraphic.car_graphics.append(self)

    def move(self):
        if self.position_index >= len(self.positions) - 1:
            self.killme = True
            return
        self.position_index = (self.position_index + 1)
        self.position = self.positions[self.position_index]

    def draw(self, screen):
        pygame.draw.rect(screen, self.color, (self.position % GRID_SIZE * CELL_SIZE, self.position // GRID_SIZE * CELL_SIZE, CELL_SIZE, CELL_SIZE))
        font = pygame.font.Font(None, 24)
        car_number_text = font.render(f"{self.car_number}", True, WHITE)
        car_number_rect = car_number_text.get_rect(center=(self.position % GRID_SIZE * CELL_SIZE + CELL_SIZE // 2, self.position // GRID_SIZE * CELL_SIZE + CELL_SIZE // 2))
        screen.blit(car_number_text, car_number_rect)

    def update(self, screen):
        self.draw(screen)
        # self.move()

    @classmethod
    def create(cls, positions):
        car_number = len(cls.car_graphics) + 1
        return cls(car_number, positions)