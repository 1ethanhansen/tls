import random
from icecream import ic
from main import set_ns_walksign, set_ew_walksign
from graphics import CarGraphic, plan
from constants import *


class World:
    def __init__(self):
        # Stores all the active cars and pedestrians
        self.cars = []
        self.pedestrians = []

        # Current info about the state
        self.state = ALL_RED
        self.state_timer = 1
        self.yellow_lights = False
        self.ewwalk = False
        self.nswalk = False

    def set_ns_walksign(self, state):
        self.nswalk = state

    def set_ew_walksign(self, state):
        self.ewwalk = state


    def select_new_state(self):
        if (not self.cars and not self.pedestrians):
            # no cars or pedestrians, stay all red
            self.state = ALL_RED
            self.state_timer = 1
        elif (not self.cars):
            # pedestrians, no cars
            self.state = random.choice(self.pedestrians[0].wanted_conditions())
            self.state_timer = 11
        elif (not self.pedestrians):
            # cars, no pedestrians
            self.state = random.choice(self.cars[0].wanted_conditions())
            self.state_timer = 11
        elif bool(random.getrandbits(1)):
            # both cars and pedestrians, True => choose from pedestrians
            self.state = random.choice(self.pedestrians[0].wanted_conditions())
            self.state_timer = 11
        else:
            # both cars and pedestrians, False => choose from cars
            self.state = random.choice(self.cars[0].wanted_conditions())
            self.state_timer = 11

    # only leave the cars and peds that don't want the current state
    def let_cars_and_peds_through(self):
        for car in self.cars:
            if self.state in car.wanted_conditions():
                CarGraphic.create(plan("car", car.location, car.goal))

        for ped in self.pedestrians:
            if self.state in ped.wanted_conditions():
                CarGraphic.create(plan("person", ped.location, ped.goal))

        self.cars = [car for car in self.cars if not (self.state in car.wanted_conditions())]
        self.pedestrians = [ped for ped in self.pedestrians if not (self.state in ped.wanted_conditions())]

    def tick(self):
        ic(self.state_timer)
        # if we've been letting traffic through and there's less than 5 seconds left, turn yellow
        if self.state != ALL_RED and self.state_timer <= 5:
            self.yellow_lights = True
        else:
            # if the lights are green, let cars and peds through
            self.yellow_lights = False
            self.let_cars_and_peds_through()


        # if we've run out of time, start a 5 second countdown for the ALL_RED state
        if self.state != ALL_RED and self.state_timer == 0:
            # then, set the state to all red
            self.state = ALL_RED
            self.state_timer = 6
        elif self.state == ALL_RED and self.state_timer == 0:
            self.select_new_state()
        
        if self.state == EW_ONLY:
            self.set_ew_walksign(True)
            self.set_ns_walksign(False)
        elif self.state == NS_ONLY:
            self.set_ew_walksign(False)
            self.set_ns_walksign(True)
        else:
            self.set_ew_walksign(False)
            self.set_ns_walksign(False)
        self.state_timer -= 1



class Car:
    def __init__(self, location, goal):
        self.location = location
        self.goal = goal

    def wanted_conditions(self):
        if self.location == NORTH:
            if self.goal == STRAIGHT:
                return [NS_ONLY, NS_THRU_RIGHT, N_ALL]
            elif self.goal == RIGHT:
                return [NS_THRU_RIGHT, N_ALL]
            elif self.goal == LEFT:
                return [NS_LEFT_TURNS, N_ALL]
        elif self.location == SOUTH:
            if self.goal == STRAIGHT:
                return [NS_ONLY, NS_THRU_RIGHT, S_ALL]
            elif self.goal == RIGHT:
                return [NS_THRU_RIGHT, S_ALL]
            elif self.goal == LEFT:
                return [NS_LEFT_TURNS, S_ALL]
        elif self.location == EAST:
            if self.goal == STRAIGHT:
                return [EW_ONLY, EW_THRU_RIGHT, E_ALL]
            elif self.goal == RIGHT:
                return [EW_THRU_RIGHT, E_ALL]
            elif self.goal == LEFT:
                return [EW_LEFT_TURNS, E_ALL]
        elif self.location == WEST:
            if self.goal == STRAIGHT:
                return [EW_ONLY, EW_THRU_RIGHT, W_ALL]
            elif self.goal == RIGHT:
                return [EW_THRU_RIGHT, W_ALL]
            elif self.goal == LEFT:
                return [EW_LEFT_TURNS, W_ALL]

class Pedestrian:
    def __init__(self, location, goal):
        self.location = location
        self.goal = goal

    def wanted_conditions(self):
        if (self.location in [NW, NE] and self.goal in [NW, NE]) or (self.location in [SW, SE] and self.goal in [SW, SE]):
            return [EW_ONLY, PEDS_NO_CARS]
        if (self.location in [NW, SW] and self.goal in [NW, SW]) or (self.location in [NE, SE] and self.goal in [NE, SE]):
           return [NS_ONLY, PEDS_NO_CARS]
